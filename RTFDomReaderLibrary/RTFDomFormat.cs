﻿using System;
using System.Text;

namespace RTFDomReaderLibrary
{
    internal class RTFDomFormat
    {
        /// <summary>
        /// format text bytes
        /// </summary>
        /// <param name="tb"></param>
        /// <returns></returns>
        internal string Format(byte[] tb)
        {
            //domString = Encoding.Default.GetString(tb);
            char[] tc = Encoding.ASCII.GetChars(tb);
            return FormatDomString(tc);
        }

        /// <summary>
        /// format dom string
        /// </summary>
        /// <param name="tc"></param>
        /// <returns></returns>
        private string FormatDomString(char[] tc)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder lb = new StringBuilder();
            int retract = 0;
            char pChar;

            foreach (char c in tc)
            {

                switch (c)
                {

                    case '{':

                        builder.Append(Indent(retract));
                        builder.Append(lb.ToString());
                        builder.Append(NewLine());

                        builder.Append(Indent(retract));
                        builder.Append(c);
                        builder.Append(NewLine());

                        retract += 1;

                        lb.Clear();

                        break;
                    case '}':
                        builder.Append(Indent(retract));
                        builder.Append(lb.ToString());
                        builder.Append(NewLine());

                        retract -= 1;

                        builder.Append(Indent(retract));
                        builder.Append(c);
                        builder.Append(NewLine());

                        lb.Clear();

                        break;
                    //case ' ':
                    case ';':
                        builder.Append(Indent(retract));
                        builder.Append(lb.ToString());
                        builder.Append(c);
                        builder.Append(NewLine());

                        lb.Clear();
                        break;
                    default:
                        lb.Append(c);
                        break;
                }
                pChar = c;
            }
            return builder.ToString();
        }
        private string Indent(int t)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < t; i++)
            {
                builder.Append("\t");
            }
            return builder.ToString();
        }
        private string NewLine()
        {
            return "\r\n";
        }

        /// <summary>
        /// format dom string
        /// </summary>
        /// <param name="tb"></param>
        /// <returns></returns>
        private string FormatDomString(byte[] tb)
        {
            StringBuilder builder = new StringBuilder();
            foreach (byte b in tb)
            {
                builder.Append(b);
            }
            return builder.ToString();
        }

        /// <summary>
        /// format dom string
        /// </summary>
        /// <param name="domString"></param>
        private string FormatDomString(string domString)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(domString);
            return builder.ToString();
        }
    }
}