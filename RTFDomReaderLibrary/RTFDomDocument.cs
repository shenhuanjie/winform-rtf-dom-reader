﻿using System;
using System.IO;
using System.Text;

namespace RTFDomReaderLibrary
{
    public class RTFDomDocument
    {
        /// <summary>
        /// text bytes
        /// </summary>
        private byte[] tb;

        /// <summary>
        /// load file by name
        /// </summary>
        /// <param name="fn">file name</param>
        public void Load(string fn)
        {
            tb = File.ReadAllBytes(fn);
        }

        /// <summary>
        /// format dom string
        /// </summary>
        /// <returns>dom format string</returns>
        public string ToDomFormatString()
        {
            RTFDomFormat df = new RTFDomFormat();

            return df.Format(tb); ;
        }
    }
}
