﻿namespace RTFDomReaderForm
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnLoadRTF = new System.Windows.Forms.ToolStripButton();
            this.btnSaveRTF = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.rtbRTFSource = new System.Windows.Forms.RichTextBox();
            this.rtbRTFText = new System.Windows.Forms.RichTextBox();
            this.btnRefreshFile = new System.Windows.Forms.ToolStripButton();
            this.lblCurrentFile = new System.Windows.Forms.ToolStripLabel();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnLoadRTF,
            this.btnSaveRTF,
            this.btnRefreshFile,
            this.lblCurrentFile});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(984, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnLoadRTF
            // 
            this.btnLoadRTF.Image = ((System.Drawing.Image)(resources.GetObject("btnLoadRTF.Image")));
            this.btnLoadRTF.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLoadRTF.Name = "btnLoadRTF";
            this.btnLoadRTF.Size = new System.Drawing.Size(106, 22);
            this.btnLoadRTF.Text = "加载RTF文件...";
            this.btnLoadRTF.Click += new System.EventHandler(this.BtnLoadRTF_Click);
            // 
            // btnSaveRTF
            // 
            this.btnSaveRTF.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveRTF.Image")));
            this.btnSaveRTF.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSaveRTF.Name = "btnSaveRTF";
            this.btnSaveRTF.Size = new System.Drawing.Size(106, 22);
            this.btnSaveRTF.Text = "保存RTF文件...";
            this.btnSaveRTF.Click += new System.EventHandler(this.BtnSaveRTF_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.rtbRTFSource);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.rtbRTFText);
            this.splitContainer1.Size = new System.Drawing.Size(984, 732);
            this.splitContainer1.SplitterDistance = 492;
            this.splitContainer1.TabIndex = 1;
            // 
            // rtbRTFSource
            // 
            this.rtbRTFSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbRTFSource.Location = new System.Drawing.Point(0, 0);
            this.rtbRTFSource.Name = "rtbRTFSource";
            this.rtbRTFSource.Size = new System.Drawing.Size(492, 732);
            this.rtbRTFSource.TabIndex = 0;
            this.rtbRTFSource.Text = "";
            // 
            // rtbRTFText
            // 
            this.rtbRTFText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbRTFText.Location = new System.Drawing.Point(0, 0);
            this.rtbRTFText.Name = "rtbRTFText";
            this.rtbRTFText.Size = new System.Drawing.Size(488, 732);
            this.rtbRTFText.TabIndex = 0;
            this.rtbRTFText.Text = "";
            // 
            // btnRefreshFile
            // 
            this.btnRefreshFile.Image = global::RTFDomReaderForm.Properties.Resources.refresh;
            this.btnRefreshFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefreshFile.Name = "btnRefreshFile";
            this.btnRefreshFile.Size = new System.Drawing.Size(85, 22);
            this.btnRefreshFile.Text = "刷新文件...";
            this.btnRefreshFile.Click += new System.EventHandler(this.BtnRefreshFile_Click);
            // 
            // lblCurrentFile
            // 
            this.lblCurrentFile.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblCurrentFile.Name = "lblCurrentFile";
            this.lblCurrentFile.Size = new System.Drawing.Size(0, 22);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 757);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainForm";
            this.Text = "RTFDomReader";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnLoadRTF;
        private System.Windows.Forms.ToolStripButton btnSaveRTF;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox rtbRTFSource;
        private System.Windows.Forms.RichTextBox rtbRTFText;
        private System.Windows.Forms.ToolStripButton btnRefreshFile;
        private System.Windows.Forms.ToolStripLabel lblCurrentFile;
    }
}

