﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using RTFDomReaderLibrary;

namespace RTFDomReaderForm
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Load RTF File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnLoadRTF_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {

                    dlg.Filter = "RTF文档(*.rtf)|*.rtf";
                    dlg.CheckFileExists = true;
                    if (dlg.ShowDialog(this) == DialogResult.OK)
                    {
                        string fileName = dlg.FileName;
                        lblCurrentFile.Text = fileName;
                        rtbRTFSource.Text = LoadFile(fileName);
                        Console.WriteLine(rtbRTFSource.Text);



                        rtbRTFText.Text = ParseFile(fileName);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show(ErrorTypes.FILE_INVALID);
            }
        }

        /// <summary>
        /// parse file
        /// </summary>
        /// <param name="fn"></param>
        /// <returns></returns>
        private string ParseFile(string fn)
        {
            RTFDomDocument doc = new RTFDomDocument();
            doc.Load(fn);
            return doc.ToDomFormatString();
        }

        /// <summary>
        /// load file
        /// </summary>
        /// <param name="fn"></param>
        /// <returns></returns>
        private string LoadFile(string fn)
        {
            byte[] tb = File.ReadAllBytes(fn);
            return Encoding.Default.GetString(tb);
        }

        /// <summary>
        /// Parse RTF File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnParseRTF_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    dlg.Filter = "RTF文档(*.rtf)|*.rtf";
                    dlg.CheckFileExists = true;
                    if (dlg.ShowDialog(this) == DialogResult.OK)
                    {
                        string fn = dlg.FileName;

                        RTFDomDocument doc = new RTFDomDocument();

                        doc.Load(fn);
                        rtbRTFSource.Text = doc.ToDomFormatString();
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show(ErrorTypes.FILE_INVALID);
            }
        }

        /// <summary>
        /// Save RTF File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSaveRTF_Click(object sender, EventArgs e)
        {
            try
            {
                using (SaveFileDialog dlg = new SaveFileDialog())
                {
                    dlg.Title = "请选择保存文件路径";
                    dlg.Filter = "RTF文档(*.rtf)|*.rtf";
                    dlg.OverwritePrompt = true;  //是否覆盖当前文件
                    dlg.RestoreDirectory = true;  //还原目录
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        string fn = dlg.FileName;
                        //byte[] bytes = Encoding.Default.GetBytes(rtbRTFSource.Rtf);
                        //if (this.IS_RTF)
                        //{
                        string rtfText = rtbRTFSource.Text.Replace("\r\n", "").Replace("\t", "");
                        byte[] bytes = Encoding.Default.GetBytes(rtfText);
                        //}
                        FileStream fs = new FileStream(fn, FileMode.Create, FileAccess.Write);
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Flush();
                        fs.Close();
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show(ErrorTypes.FILE_INVALID);
            }
        }

        private void BtnRefreshFile_Click(object sender, EventArgs e)
        {
            string fileName = lblCurrentFile.Text;
            rtbRTFSource.Text = LoadFile(fileName);
            rtbRTFText.Text = ParseFile(fileName);
        }
    }
}
